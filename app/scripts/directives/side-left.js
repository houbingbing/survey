define([
    '../directives/directives',
    '../cons/simpleCons'
], function (mod, cons) {

    mod
        .directive('sideMenu', function ($rootScope, $state, $http, $uibModal, $filter, widget, $templateCache) {
            return {
                restrict: 'EA',
                replace: true,
                //require: '?ngModel',
                scope: true,
                template: $templateCache.get('app/' + cons.DIRECTIVE_PATH + 'side-left/side-menu.html'),
                link: function ($scope, $element, $attrs) {
                    $scope.cons = cons;
                    $rootScope.hjm = {};
                    $rootScope.hjm.menus = [
                        {
                            icon: "fa fa-th-list",
                            id: 18,
                            key: "1",
                            name: "活动列表",
                            pid: "0",
                            route: "activity.list",
                            sort: "30",
                            type: "1",
                            childs: [
                                {
                                    icon: "1",
                                    id: 1,
                                    key: "1",
                                    name: "活动列表",
                                    pid: "1",
                                    route: "activity.list",
                                    sort: "5",
                                    type: "1",
                                }
                            ]
                        },
                        {
                            // icon: "fa fa-list-alt",
                            icon: "fa fa-th-list",
                            id: 18,
                            key: "1",
                            name: "测评列表",
                            pid: "0",
                            route: "plan.list",
                            sort: "30",
                            type: "1",
                            childs: [
                                {
                                    icon: "1",
                                    id: 2,
                                    key: "1",
                                    name: "测评列表",
                                    pid: "2",
                                    route: "plan.list",
                                    sort: "10",
                                    type: "1",
                                }
                            ]
                        },
                        {
                            // icon: "fa fa-th-large",
                            icon: "fa fa-th-list",
                            id: 18,
                            key: "1",
                            name: "题目列表",
                            pid: "0",
                            route: "question.list",
                            sort: "30",
                            type: "1",
                            childs: [
                                {
                                    icon: "1",
                                    id: 2,
                                    key: "1",
                                    name: "题目列表",
                                    pid: "2",
                                    route: "question.list",
                                    sort: "10",
                                    type: "1",
                                }
                            ]
                        },
                        {
                            // icon: "fa fa-comments",
                            icon: "fa fa-th-list",
                            id: 18,
                            key: "1",
                            name: "评语列表",
                            pid: "0",
                            route: "evaluation.list",
                            sort: "30",
                            type: "1",
                            childs: [
                                {
                                    icon: "1",
                                    id: 2,
                                    key: "1",
                                    name: "评语列表",
                                    pid: "2",
                                    route: "evaluation.list",
                                    sort: "10",
                                    type: "1",
                                }
                            ]
                        },
                        {
                            // icon: "fa  fa-list-ol",
                            icon: "fa fa-th-list",
                            id: 18,
                            key: "1",
                            name: "推广列表",
                            pid: "0",
                            route: "ad.list",
                            sort: "30",
                            type: "1",
                            childs: [
                                {
                                    icon: "1",
                                    id: 2,
                                    key: "1",
                                    name: "推广列表",
                                    pid: "2",
                                    route: "ad.list",
                                    sort: "10",
                                    type: "1",
                                }
                            ]
                        },
                        {
                            // icon: "fa fa-sort-alpha-asc",
                            icon: "fa fa-th-list",
                            id: 18,
                            key: "1",
                            name: "维度列表",
                            pid: "0",
                            route: "category.list",
                            sort: "30",
                            type: "1",
                            childs: [
                                {
                                    icon: "1",
                                    id: 2,
                                    key: "1",
                                    name: "维度列表",
                                    pid: "2",
                                    route: "category.list",
                                    sort: "10",
                                    type: "1",
                                }
                            ]
                        },
                        {
                            // icon: "fa fa-sort-numeric-asc",
                            icon: "fa fa-th-list",
                            id: 18,
                            key: "1",
                            name: "分类列表",
                            pid: "0",
                            route: "classification.list",
                            sort: "30",
                            type: "1",
                            childs: [
                                {
                                    icon: "1",
                                    id: 2,
                                    key: "1",
                                    name: "分类列表",
                                    pid: "2",
                                    route: "classification.list",
                                    sort: "10",
                                    type: "1",
                                }
                            ]
                        },
                        {
                            // icon: "fa fa-sort-amount-asc",
                            icon: "fa fa-th-list",
                            id: 18,
                            key: "1",
                            name: "附加信息列表",
                            pid: "0",
                            route: "attachment.list",
                            sort: "30",
                            type: "1",
                            childs: [
                                {
                                    icon: "1",
                                    id: 2,
                                    key: "1",
                                    name: "附加信息列表",
                                    pid: "2",
                                    route: "attachment.list",
                                    sort: "10",
                                    type: "1",
                                }
                            ]
                        },
                        {
                            icon: "fa fa-tag",
                            id: 18,
                            key: "1",
                            name: "ICON",
                            pid: "0",
                            route: "icon",
                            sort: "30",
                            type: "1",
                            childs: [
                                {
                                    icon: "1",
                                    id: 2,
                                    key: "1",
                                    name: "ICON",
                                    pid: "2",
                                    route: "icon",
                                    sort: "10",
                                    type: "1",
                                }
                            ]
                        },
                    ]
                    // console.log($rootScope.hjm.menus);
                    $rootScope.current_state = $state.current.name;
                    $scope.menus = ($rootScope.hjm || {}).menus;
                    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
                        if ($rootScope.hjm && $rootScope.hjm.menus) {
                            angular.forEach($rootScope.hjm.menus, function (menuval, key) {
                                if (menuval.route == $state.current.name.split('main.')[1]) {
                                    $scope.mainmenu = 'main.' + menuval.route;
                                }
                            });
                        }
                    });
                }
            };
        })
        .directive('sideWidgets', function ($rootScope, $state, $http, $uibModal, $filter, widget, $templateCache) {
            return {
                restrict: 'EA',
                // replace: true,
                //require: '?ngModel',
                scope: true,
                template: $templateCache.get('app/' + cons.DIRECTIVE_PATH + 'side-left/side-widgets.html'),
                link: function ($scope, $element, $attrs) {

                }
            };
        })

})
;
