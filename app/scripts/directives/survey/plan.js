define([
    '../../directives/directives',
    '../../cons/simpleCons'
], function (mod, simpleCons) {
    mod
        .directive('surveyPlanDownload', function ($rootScope, $templateCache, $filter, $compile, widget) {
            return {
                restrict: 'AE',
                replace: false,
                scope: {
                    data: '=',
                },
                template: '<p><div export-run data="item"></div></p>',
                // template: '<p class="survey-category-download-html"></p>',
                link: function ($scope, $element, $attrs) {
                    // console.log($scope.data.id);
                    $scope.item = {
                        command: "export:testing",
                        condition: {
                            end_time: {
                                name: "结束时间", type: "date", options: [], defaultValue: []
                            },
                            start_time: {
                                name: "开始时间", type: "date", options: [], defaultValue: []
                            },
                            plan_id: {
                                name: "测评ID", type: "text", options: [], defaultValue: [], default_val: $scope.data.id,
                            }
                        },
                        desc: "导出测评数据"
                    };
                }
            }
        })
        .directive('surveyPlanEdit', function ($rootScope, $templateCache, $filter, $compile, widget) {
            return {
                multiElement: true,
                restrict: 'AE',
                replace: false,
                scope: {
                    data: '=',
                },
                template: '',
                link: function ($scope, $element, $attrs) {
                    var content = '<a class="btn btn-success btn-rounded btn-sm" ui-sref="main.plan.update({id:' + $scope.data.id + '})">编辑</a>';
                    $element.html(content);
                    $compile($element.contents())($scope);
                }
            }
        })
        .directive('surveyPlanChangeStatus', function ($templateCache, $filter, $compile, widget) {
            return {
                multiElement: true,
                restrict: 'AE',
                replace: false,
                scope: {
                    data: '=',
                },
                template: '',
                link: function ($scope, $element, $attrs) {
                    var status_text = '';
                    var click_text = '';
                    var class_text = '';
                    var status_title = '';
                    if ($scope.data.status == 1) {
                        status_title = '停用';
                        status_text = 'ng-bind="\'停用\'"';
                        class_text = 'ng-class={\"btn-warning\":true} ';
                        click_text = 'ng-click="change(2);"';
                        $scope.show_text = true;
                    } else if ($scope.data.status == 2) {
                        status_title = '正常';
                        status_text = 'ng-bind="\'正常\'"';
                        class_text = 'ng-class={\"btn-primary\":true} ';
                        click_text = 'ng-click="change(1);"';
                        $scope.show_text = true;
                    }
                    $scope.change = function (status) {
                        if (confirm('确认修改为' + status_title + '状态?')) {
                            widget.ajaxRequest({
                                domain: 'survey_domain',
                                url: '/plans/' + ($scope.data.id || 0) + '/status',
                                method: 'put',
                                scope: $scope,
                                data: {status: status},
                                success: function (json) {
                                    widget.msgToast('修改成功,请刷新查看');
                                    $scope.$parent.$parent.searchAction();
                                }
                            })
                        }
                    }
                    var content = '<a class="btn btn-rounded btn-sm"' + class_text + status_text + click_text + '></a>';
                    $element.html(content);
                    $compile($element.contents())($scope);
                }
            }
        })
});
