define([], function () {
    var rtn = {
        evaluationList: {
            columns: [
                {name: 'ID', field: 'id', className: 'text-center'},
                {name: '标注', field: 'remark'},
                {name: '更新时间', field: 'updated_at'},
                {
                    name: '操作',className: 'text-center',
                    fieldDirective: '<a class="btn btn-success btn-rounded btn-sm" ui-sref="main.evaluation.update({id:item.id})">编辑</a>&nbsp;&nbsp;&nbsp;'
                    // +'<span survey-evaluation-change-status data="item" ></span>'
                },
            ],
            config: {
                title: '评语列表',
                domain: 'survey_domain',
                api: '/evaluations',
                rowItemName: 'item',
                searchSupport: false,
                searchItems: [],
                preSelectionSearch: {
                    // status: 1,
                },
                paginationSupport: true,
                pageInfo: {
                    count: 20,
                    page: 1,
                    maxSize: 5, //最大展示页，默认3
                    // showPageGoto: false //属性为true将显示前往第几页。
                },
                route: [
                    {value: 'main.evaluation.add', text: '新增评语库'},
                ]
            },
        }
    }
    return rtn;
});