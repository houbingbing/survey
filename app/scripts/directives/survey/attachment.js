define([
    '../../directives/directives',
    '../../cons/simpleCons'
], function (mod, simpleCons) {
    mod
        .directive('attachmentAdd', function ($rootScope, $templateCache, $filter, $compile, widget, $uibModal, $timeout) {
            return {
                restrict: 'AE',
                replace: true,
                scope: {
                    data: '=',
                },
                template: '<a class="btn btn-success btn-rounded btn-sm pull-right" ng-click="show_attachment_add()" >新增测评附加信息</a>',
                link: function ($scope, $element, $attrs) {
                    var supscope = $scope;
                    $scope.show_attachment_add = function () {
                        var modalInstance = $uibModal.open({
                                template: '<div modal-panel title="title" tmpl="tmpl"></div>',
                                controller: function ($scope, $uibModalInstance) {
                                    $scope.title = '新增测评附加信息';
                                    $scope.tmpl = $templateCache.get('app/' + simpleCons.survey_path + 'attachment/update.html');
                                    $scope.submit = function () {
                                        if (!$scope.param.title) {
                                            widget.msgToast('没有填写附加信息');
                                            return false;
                                        }
                                        widget.ajaxRequest({
                                            domain: 'survey_domain',
                                            url: '/attachments',
                                            method: 'POST',
                                            scope: $scope,
                                            data: $scope.param,
                                            success: function (json) {
                                                widget.msgToast('附加信息维护成功,请刷新查看');
                                                supscope.$parent.$parent.searchAction();
                                                $rootScope.reset();
                                                $scope.cancel();
                                            },
                                            failure: function (json) {
                                                widget.msgToast(json.message);
                                                $scope.cancel();
                                            }
                                        })
                                    }
                                    $scope.cancel = function () {
                                        $uibModalInstance.dismiss('cancel');
                                    };
                                },
                                size: ''
                            }
                        );
                    }
                }
            }
        })
        .directive('attachmentEdit', function ($rootScope, $templateCache, $filter, $compile, widget, $uibModal, $timeout) {
            return {
                restrict: 'AE',
                replace: true,
                scope: {
                    data: '=',
                },
                template: '<a class="btn btn-success btn-rounded btn-sm" ng-click="show_attachment_edit()" >更新</a>',
                link: function ($scope, $element, $attrs) {
                    var supscope = $scope;
                    $scope.show_attachment_edit = function () {
                        var modalInstance = $uibModal.open({
                                template: '<div modal-panel title="title" tmpl="tmpl"></div>',
                                controller: function ($scope, $uibModalInstance) {
                                    $scope.title = '更新测评附加信息';
                                    $scope.tmpl = $templateCache.get('app/' + simpleCons.survey_path + 'attachment/update.html');
                                    $timeout(function () {
                                        $scope.param = supscope.data;
                                    }, 0);
                                    $scope.submit = function () {
                                        if (!$scope.param.title) {
                                            widget.msgToast('没有填写附加信息');
                                            return false;
                                        }
                                        widget.ajaxRequest({
                                            domain: 'survey_domain',
                                            url: '/attachments/' + supscope.data.id,
                                            method: 'PUT',
                                            scope: $scope,
                                            data: $scope.param,
                                            success: function (json) {
                                                widget.msgToast('附加信息维护成功,请刷新查看');
                                                supscope.$parent.$parent.searchAction();
                                                $rootScope.reset();
                                                $scope.cancel();
                                            },
                                            failure: function (json) {
                                                widget.msgToast(json.message);
                                                $scope.cancel();
                                            }
                                        })
                                    }
                                    $scope.cancel = function () {
                                        $uibModalInstance.dismiss('cancel');
                                    };
                                },
                                size: ''
                            }
                        );
                    }
                }
            }
        })
        .directive('attachmentDel', function ($rootScope, $templateCache, $filter, $compile, widget, $uibModal, $timeout) {
            return {
                restrict: 'AE',
                replace: true,
                scope: {
                    data: '=',
                },
                template: '<a class="btn btn-danger btn-rounded btn-sm" ng-click="show_attachment_del()" >删除</a>',
                link: function ($scope, $element, $attrs) {
                    var supscope = $scope;
                    $scope.show_attachment_del = function () {
                        var modalInstance = $uibModal.open({
                                template: '<div modal-panel title="title" tmpl="tmpl"></div>',
                                controller: function ($scope, $uibModalInstance) {
                                    $scope.title = '删除测评附加信息';
                                    $scope.tmpl = $templateCache.get('app/' + simpleCons.survey_path + 'attachment/delete.html');
                                    $timeout(function () {
                                        $scope.param = supscope.data;
                                    }, 0);
                                    $scope.submit = function () {
                                        widget.ajaxRequest({
                                            domain: 'survey_domain',
                                            url: '/attachments/' + supscope.data.id + '/status/2',
                                            method: 'PUT',
                                            scope: $scope,
                                            // data: {},
                                            success: function (json) {
                                                widget.msgToast('删除附加信息成功,请刷新查看');
                                                supscope.$parent.$parent.searchAction();
                                                $rootScope.reset();
                                                $scope.cancel();
                                            },
                                            failure: function (json) {
                                                widget.msgToast(json.message);
                                                $scope.cancel();
                                            }
                                        })
                                    }
                                    $scope.cancel = function () {
                                        $uibModalInstance.dismiss('cancel');
                                    };
                                },
                                size: ''
                            }
                        );
                    }
                }
            }
        })
});
