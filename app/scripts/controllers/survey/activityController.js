// This is a file copied by your subgenerator
define([
    './../controllers'
    , '../../cons/simpleCons'
], function (mod, con) {
    mod.controller('activity.updateController', updateController);

    updateController.$injector = ['$scope', '$http', '$rootScope', '$uibModal', '$state', '$stateParams', 'widget', '$filter', '$timeout'];

    function updateController($scope, $http, $rootScope, $uibModal, $state, $stateParams, widget, $filter, $timeout) {
        //
        $scope.plan_id = '';
        $scope.plan_name = '';
        $scope.plan_age_min = '';
        $scope.plan_age_max = '';
        //
        $scope.attachment_id = '';
        $scope.attachment_title = '';

        if ($stateParams.id) {
            widget.ajaxRequest({
                url: con.survey_domain + '/activities/' + $stateParams.id,
                method: 'get',
                scope: $scope,
                data: {},
                success: function (json) {
                    json.data.order_by = Number(json.data.order_by);
                    $scope.param = angular.copy(json.data);
                }
            })
        }

        // 添加 问题
        $scope.add_plan = function () {
            if (!$scope.plan_id || !$scope.plan_name) {
                widget.msgToast('测评ID未填写或者题目不存在,点击取消');
                return false;
            }
            var is_plan_repeat = false;
            angular.forEach($scope.param.plans, function (v, k) {
                if (v.plan_id == $scope.plan_id) {
                    is_plan_repeat = true;
                }
            })
            if (is_plan_repeat) {
                widget.msgToast('测评已存在,点击取消');
                return false;
            } else {
                if (!$scope.param.plans) {
                    $scope.param.plans = [];
                }
                $scope.param.plans.push({
                    plan_id: $scope.plan_id,
                    plan: {
                        name: $scope.plan_name,
                        age_min: $scope.plan_age_min,
                        age_max: $scope.plan_age_max,
                    },

                });
                $scope.plan_id = '';
                $scope.plan_name = '';
                $scope.plan_age_min = '';
                $scope.plan_age_max = '';
            }
        }
        $scope.search_plan_id = function () {
            if (!$scope.plan_id) {
                widget.msgToast('没有输入测评ID,点击取消');
                return false;
            }
            widget.ajaxRequest({
                url: con.survey_domain + '/plans',
                method: 'GET',
                data: {
                    id: $scope.plan_id,
                    page: 1,
                    count: 1,
                    status: 1,
                },
                success: function (json) {
                    if (json.data[0]) {
                        $scope.plan_name = json.data[0].name || '无测评描述';
                        $scope.plan_age_min = json.data[0].age_min;
                        $scope.plan_age_max = json.data[0].age_max;
                        $scope.add_plan();
                    } else {
                        $scope.plan_answer = '';
                        widget.msgToast('未查询到测评ID,点击取消');
                    }
                }
            })
        }


        // 添加 附加信息
        $scope.add_attachment = function () {
            if (!$scope.attachment_id || !$scope.attachment_title) {
                widget.msgToast('附加信息ID未填写或者题目不存在,点击取消');
                return false;
            }
            var is_attachment_repeat = false;
            angular.forEach($scope.param.attachments, function (v, k) {
                if (v.attachment_id == $scope.attachment_id) {
                    is_attachment_repeat = true;
                }
            })
            if (is_attachment_repeat) {
                widget.msgToast('附加信息已存在,点击取消');
                return false;
            } else {
                if (!$scope.param.attachments) {
                    $scope.param.attachments = [];
                }
                $scope.param.attachments.push({
                    attachment_id: $scope.attachment_id,
                    attachment: {
                        title: $scope.attachment_title,
                    },

                });
                $scope.attachment_id = '';
                $scope.attachment_title = '';
            }
        }

        $scope.search_attachment_id = function () {
            if (!$scope.attachment_id) {
                widget.msgToast('没有输入附加信息ID,点击取消');
                return false;
            }
            widget.ajaxRequest({
                url: con.survey_domain + '/attachments',
                method: 'GET',
                data: {
                    id: $scope.attachment_id,
                    page: 1,
                    count: 1,
                },
                success: function (json) {
                    if (json.data[0]) {
                        $scope.attachment_id = json.data[0].id;
                        $scope.attachment_title = json.data[0].title || '无附加信息描述';
                        $scope.add_attachment();
                    } else {
                        $scope.attachment_title = '';
                        widget.msgToast('未查询到附加信息D,点击取消');
                    }
                }
            })
        }
        $scope.submit = function (status) {
            widget.ajaxRequest({
                url: con.survey_domain + '/activities' + ($stateParams.id ? ('/' + $stateParams.id) : ''),
                method: $stateParams.id ? 'PUT' : 'POST',
                scope: $scope,
                data: $scope.param,
                success: function (json) {
                    widget.msgToast('发布成功！');
                    $state.go(con.state.main + '.activity.list');
                }
            })
        }
    };
});
