define([
    './filters',
    '../cons/simpleCons'
], function (mod, simpleCons) {
    mod
    // 限制类型，1单次，2多次，3无限制
        .filter('survey_limit_type', [function () {
            return function (val) {
                var result = val;
                val = val + '';
                switch (val) {
                    case "2":
                        result = "限制";
                        break;
                    case "3":
                        result = "无限制";
                        break;
                }

                return result;
            }
        }])
    ;
});