define([
    '../../directives/directives',
    '../../cons/simpleCons'
], function (mod, simpleCons) {
    mod
        .directive('surveyClassificationAdd', function ($rootScope, $templateCache, $filter, $compile, widget, $uibModal, $timeout) {
            return {
                restrict: 'AE',
                replace: true,
                scope: {
                    data: '=',
                },
                template: '<a class="btn btn-success btn-rounded btn-sm pull-right" ng-click="show_survey_category_add()" >新增测评分类</a>',
                link: function ($scope, $element, $attrs) {
                    var supscope = $scope;
                    $scope.show_survey_category_add = function () {
                        var modalInstance = $uibModal.open({
                                template: '<div modal-panel title="title" tmpl="tmpl"></div>',
                                controller: function ($scope, $uibModalInstance) {
                                    $scope.title = '新增测评分类';
                                    $scope.tmpl = '<form class="form-horizontal" name="FormBody" novalidate >' +
                                        '<div form-input text="分类" ng-model="param.name"></div>' +
                                        '<a class="btn btn-success btn-rounded pull-right" ng-click="submit()">确定</a>' +
                                        '</form>';
                                    $scope.submit = function () {
                                        if (!$scope.param.name) {
                                            widget.msgToast('没有填写分类');
                                            return false;
                                        }
                                        widget.ajaxRequest({
                                            domain: 'survey_domain',
                                            url: '/classifications',
                                            method: 'POST',
                                            scope: $scope,
                                            data: $scope.param,
                                            success: function (json) {
                                                widget.msgToast('分类维护成功,请刷新查看');
                                                $scope.$$prevSibling.$$childHead.$$nextSibling.$$nextSibling.$$nextSibling.$$childHead.$$childHead.searchAction()
                                                $rootScope.reset();
                                                $scope.cancel();
                                            },
                                            failure: function (json) {
                                                widget.msgToast(json.message);
                                                $scope.cancel();
                                            }
                                        })
                                    }
                                    $scope.cancel = function () {
                                        $uibModalInstance.dismiss('cancel');
                                    };
                                },
                                size: ''
                            }
                        );
                    }
                }
            }
        })
        .directive('surveyClassificationEdit', function ($rootScope, $templateCache, $filter, $compile, widget, $uibModal, $timeout) {
            return {
                restrict: 'AE',
                replace: true,
                scope: {
                    data: '=',
                },
                template: '<a class="btn btn-success btn-rounded btn-sm" ng-click="show_survey_category_edit()" >更新</a>',
                link: function ($scope, $element, $attrs) {
                    var supscope = $scope;
                    $scope.show_survey_category_edit = function () {
                        var modalInstance = $uibModal.open({
                                template: '<div modal-panel title="title" tmpl="tmpl"></div>',
                                controller: function ($scope, $uibModalInstance) {
                                    $scope.title = '更新测评分类';
                                    $scope.tmpl = '<form class="form-horizontal" name="FormBody" novalidate >' +
                                        '<div form-input text="ID" ng-model="param.id" ng-disabled="true"></div>' +
                                        '<div form-input text="分类" ng-model="param.name"></div>' +
                                        '<a class="btn btn-success btn-rounded pull-right" ng-click="submit()">确定</a>' +
                                        '</form>';
                                    $timeout(function () {
                                        // $scope.param = {id: supscope.data.id};
                                        $scope.param = supscope.data;
                                    }, 0);
                                    $scope.submit = function () {
                                        if (!$scope.param.name) {
                                            widget.msgToast('没有填写分类');
                                            return false;
                                        }
                                        widget.ajaxRequest({
                                            domain: 'survey_domain',
                                            url: '/classifications/' + supscope.data.id,
                                            method: 'PUT',
                                            scope: $scope,
                                            data: $scope.param,
                                            success: function (json) {
                                                widget.msgToast('分类维护成功,请刷新查看');
                                                $scope.$$prevSibling.$$childHead.$$nextSibling.$$nextSibling.$$nextSibling.$$childHead.$$childHead.searchAction()
                                                $rootScope.reset();
                                                $scope.cancel();
                                            },
                                            failure: function (json) {
                                                widget.msgToast(json.message);
                                                $scope.cancel();
                                            }
                                        })
                                    }
                                    $scope.cancel = function () {
                                        $uibModalInstance.dismiss('cancel');
                                    };
                                },
                                size: ''
                            }
                        );
                    }
                }
            }
        })
        .directive('surveyClassificationDel', function ($rootScope, $templateCache, $filter, $compile, widget, $uibModal, $timeout) {
            return {
                restrict: 'AE',
                replace: true,
                scope: {
                    data: '=',
                },
                template: '<a class="btn btn-danger btn-rounded btn-sm" ng-click="show_survey_category_del()" >删除</a>',
                link: function ($scope, $element, $attrs) {
                    var supscope = $scope;
                    $scope.show_survey_category_del = function () {
                        var modalInstance = $uibModal.open({
                                template: '<div modal-panel title="title" tmpl="tmpl"></div>',
                                controller: function ($scope, $uibModalInstance) {
                                    $scope.title = '删除测评分类';
                                    $scope.tmpl = '<form class="form-horizontal" name="FormBody" novalidate >' +
                                        '<div form-input text="ID" ng-model="param.id" ng-disabled="true"></div>' +
                                        '<div form-input text="分类" ng-model="param.name" ng-disabled="true"></div>' +
                                        '<div form-radio text="类型" ng-model="param.type" ng-disabled="true" default="2"' +
                                        ' source="[{text:\'系统保留\',value:\'1\'},{text:\'可编辑\',value:\'2\'}]" ></div>' +
                                        '<a class="btn btn-danger btn-rounded pull-right" ng-click="submit()">删除</a>' +
                                        '</form>';
                                    $timeout(function () {
                                        // $scope.param = {id: supscope.data.id};
                                        $scope.param = supscope.data;
                                    }, 0);
                                    $scope.submit = function () {
                                        if (!$scope.param.name) {
                                            widget.msgToast('没有填写分类');
                                            return false;
                                        }
                                        widget.ajaxRequest({
                                            domain: 'survey_domain',
                                            url: '/classifications/' + supscope.data.id,
                                            method: 'DELETE',
                                            scope: $scope,
                                            data: $scope.param,
                                            success: function (json) {
                                                widget.msgToast('删除分类成功,请刷新查看');
                                                $scope.$$prevSibling.$$childHead.$$nextSibling.$$nextSibling.$$nextSibling.$$childHead.$$childHead.searchAction()
                                                $rootScope.reset();
                                                $scope.cancel();
                                            },
                                            failure: function (json) {
                                                widget.msgToast(json.message);
                                                $scope.cancel();
                                            }
                                        })
                                    }
                                    $scope.cancel = function () {
                                        $uibModalInstance.dismiss('cancel');
                                    };
                                },
                                size: ''
                            }
                        );
                    }
                }
            }
        })
        .directive('surveyClassificationDownload', function ($rootScope, $templateCache, $filter, $compile, widget) {
            return {
                restrict: 'AE',
                replace: false,
                scope: {
                    data: '=',
                },
                template: '<div export-run data="item"></div>',
                // template: '<p class="survey-category-download-html"></p>',
                link: function ($scope, $element, $attrs) {
                    // console.log($scope.data.id);
                    $scope.item = {
                        command: "export:surveyquestion",
                        condition: {
                            category_id: {
                                name: "分类编号",
                                type: "text",
                                required: true,
                                options: [],
                                defaultValue: [],
                                default_val: $scope.data.id,
                            }
                        },
                        desc: "测评题库",
                    };
                }
            }
        })
});
