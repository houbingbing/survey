<?php
header('Access-Control-Allow-Origin: *'); //设置http://www.baidu.com允许跨域访问
header('Access-Control-Allow-Headers: X-Requested-With,X_Requested_With'); //设置允许的跨域header
date_default_timezone_set("Asia/chongqing");
error_reporting(E_ERROR);
//header("Content-Type: text/html; charset=utf-8");
header("Content-Type: application/x-www-form-urlencoded");

$request_body = json_decode(file_get_contents('php://input'));

$param = array('content' => 'fdipzone blog');

$rsp_code = 0;
$result_data = array();
foreach ($request_body as $key => $url) {
    $output = postData($url, http_build_query($param));
    $output_code = json_decode($output)->code;
    $output_data = json_decode($output)->data;
    if ($output_code == 0) {
        $result_data[$key] = $output_data;
    } else {
        $rsp_code = 500;
        $result_data[$key] = '';
    }
}

if (!$rsp_code == 500) {
    $ret = array(
        'code' => 0,
        'message' => '',
        'data' => $result_data,
    );
    echo json_encode($ret, true);
    return;
} else {
    $ret = array(
        'code' => 500,
        'message' => '网络请求错误',
        'data' => $result_data,
    );
    echo json_encode($ret, true);
}


function postData($url, $data)
{
    $ch = curl_init();
    $timeout = 300;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERPWD, $_SERVER['PHP_AUTH_USER'] . ':' . $_SERVER['PHP_AUTH_PW']);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;

}

?>