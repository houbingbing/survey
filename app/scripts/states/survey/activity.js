// This is a file copied by your subgenerator
/**
 * 默认的产品首页和默认的跳转规则
 */
define([
        '../states'
        , '../../cons/simpleCons'
        , '../../controllers/survey/activityController'
    ],
    function (stateModule, cons) {
        stateModule.config(
            ['$stateProvider', '$urlRouterProvider',
                function ($stateProvider, $urlRouterProvider) {
                    $stateProvider
                        .state(cons.state.main + '.activity', {
                            url: "/activity",
                            templateProvider: function ($templateCache) {
                                return $templateCache.get('app/' + cons.main_path + 'container.html');
                            }
                        })
                        .state(cons.state.main + '.activity.list', {
                            url: "/list",
                            views: {
                                "": {
                                    // controller: 'accountController'
                                    templateProvider: function ($templateCache) {
                                        return '<div hjm-grid modid="activityList" config="config" columns="columns"></div>';
                                    }
                                }
                            }
                        })
                        .state(cons.state.main + '.activity.add', {
                            url: "/add",
                            views: {
                                "": {
                                    controller: 'activity.updateController',
                                    templateProvider: function ($templateCache) {
                                        return $templateCache.get('app/' + cons.survey_path + 'activity/update.html');
                                    }
                                }
                            }
                        })
                        .state(cons.state.main + '.activity.update', {
                            url: "/update.html/:id",
                            views: {
                                "": {
                                    controller: 'activity.updateController',
                                    templateProvider: function ($templateCache) {
                                        return $templateCache.get('app/' + cons.survey_path + 'activity/update.html');
                                    }
                                }
                            }
                        })
                }
            ]);
    })
