// This is a file copied by your subgenerator
define([
    './../controllers'
    , '../../cons/simpleCons'
], function (mod, con) {
    mod.controller('plan.updateController', updateController);

    updateController.$injector = ['$scope', '$http', '$rootScope', '$uibModal', '$state', '$stateParams', 'widget', '$filter', '$timeout', '$q'];

    function updateController($scope, $http, $rootScope, $uibModal, $state, $stateParams, widget, $filter, $timeout, $q) {

        $scope.question_answer = '';
        $scope.question_category_id = '';
        $scope.question_classification_id = '';
        $scope.question_category = '';
        $scope.question_classification = '';

        var init = function () {
            widget.ajaxRequest({
                url: con.survey_domain + '/plans/' + $stateParams.id,
                method: 'get',
                scope: $scope,
                data: {},
                success: function (json) {
                    json.data.order_by = Number(json.data.order_by);
                    $scope.param = angular.copy(json.data);
                }
            })
        }
        if ($stateParams.id) {
            if (!$rootScope.common['survey_category']) {
                var promise = $rootScope.reset();
                promise.then(function (json) {
                    init();
                }, function (error) {
                    widget.msgToast(error);
                }, function (notify) {
                    console.log('promise 的执行状态 notify :  ' + notify);
                });
            } else {
                init();
            }
        }

        // 添加 问题
        $scope.add_question = function () {
            if (!$scope.question_id || !$scope.question_answer) {
                widget.msgToast('问题ID未填写或者题目不存在,点击取消');
                return false;
            }
            var is_question_repeat = false;
            angular.forEach($scope.param.questions, function (v, k) {
                if (v.question_id == $scope.question_id) {
                    is_question_repeat = true;
                }
            })
            if (is_question_repeat) {
                widget.msgToast('题目已存在,点击取消');
                return false;
            } else {
                if (!$scope.param.questions) {
                    $scope.param.questions = [];
                }
                $scope.param.questions.push({
                    order_by: '0',
                    question_id: $scope.question_id,
                    question: {
                        category_id: $scope.question_category_id,
                        classification_id: $scope.question_classification_id,
                        category: {name: $scope.question_category},
                        classification: {name: $scope.question_classification},
                    },

                });
                $scope.question_id = '';
                $scope.question_answer = '';
                $scope.question_category_id = '';
                $scope.question_classification_id = '';
                $scope.question_category = '';
                $scope.question_classification = '';
            }
        }

        // 排序字段 赋值
        $scope.$watch('param.questions', function (val) {
            angular.forEach(val, function (v, k) {
                v.order_by = k + 1;
            })
        }, true);

        // 查询问题ID 并且添加进题库列表

        $scope.search_question_id = function () {
            if (!$scope.question_id) {
                widget.msgToast('没有输入问题ID,点击取消');
                return false;
            }
            widget.ajaxRequest({
                url: con.survey_domain + '/questions',
                method: 'GET',
                data: {
                    id: $scope.question_id,
                    page: 1,
                    count: 1,
                },
                success: function (json) {
                    if (json.data[0]) {
                        $scope.question_answer = json.data[0].answer || '无题目描述';
                        $scope.question_category_id = json.data[0].category_id || '';
                        $scope.question_classification_id = json.data[0].classification_id || '';
                        $scope.question_category = json.data[0].category && json.data[0].category.name || '无题目维度';
                        $scope.question_classification = json.data[0].classification && json.data[0].classification.name || '无题目分类';
                        $scope.add_question();
                    } else {
                        $scope.question_answer = '';
                        widget.msgToast('未查询到题目ID,点击取消');
                    }
                }
            })
        }

        $scope.plan_category_tmp = [];  //  当前的选择的维度有哪些 用于评语
        $scope.reset_plan_category_tmp = function () {
            var that = null;
            if ($scope.param && $scope.param.question_from == 1) {
                that = $scope.param.categories;
            } else if ($scope.param && $scope.param.question_from == 2) {
                that = $scope.param.questions;
            }
            if (!that) {
                return false;
            }
            // console.log(that);
            $scope.plan_category_tmp = [];
            var obj = {};
            angular.forEach(that, function (val, key) {
                var category_id = ($scope.param.question_from == 1) ? val.category_id : (val.question && val.question.category_id || '');
                if (!obj['tmp' + category_id]) {
                    angular.forEach($rootScope.common['survey_category'], function (v, k) {
                        if (v.value == category_id) {
                            var is_add_tmp = true;
                            angular.forEach($scope.plan_category_tmp, function (tmp_val, tmp_key) {
                                if (tmp_val.value == v.value) {
                                    is_add_tmp = false;
                                }
                            })
                            if (is_add_tmp) {
                                $scope.plan_category_tmp.push({value: v.value, text: v.text});
                            }
                        }
                    });
                    obj['tmp' + val.category_id] = 1;
                }
            });
        }

        $scope.$watch('param.categories', function (that) {
            $scope.reset_plan_category_tmp();
        }, true);
        $scope.$watch('param.questions', function (that) {
            $scope.reset_plan_category_tmp();
        }, true);
        $scope.$watch('param.question_from', function (value) {
            $scope.reset_plan_category_tmp();
        }, true);

        $scope.submit = function (status) {
            widget.ajaxRequest({
                url: con.survey_domain + '/plans' + ($stateParams.id ? ('/' + $stateParams.id) : ''),
                method: $stateParams.id ? 'PUT' : 'POST',
                scope: $scope,
                data: $scope.param,
                success: function (json) {
                    widget.msgToast('发布成功！');
                    $state.go(con.state.main + '.plan.list');
                }
            })
        }
    };
});
