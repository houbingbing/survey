// This is a file copied by your subgenerator
define([
    './../controllers'
    , '../../cons/simpleCons'
], function (mod, con) {
    mod
        .controller('evaluation.updateController', updateController)

    updateController.$injector = ['$scope', '$http', '$rootScope', '$uibModal', '$state', '$stateParams', 'widget', '$filter'];
    function updateController($scope, $http, $rootScope, $uibModal, $state, $stateParams, widget, comfunc, $filter) {
        if ($stateParams.id) {
            widget.ajaxRequest({
                domain: 'survey_domain',
                url: '/evaluations/' + $stateParams.id,
                method: 'get',
                scope: $scope,
                data: {},
                success: function (json) {
                    $scope.param = angular.copy(json.data);
                }
            })
        }

        $scope.submit = function (status) {
            widget.ajaxRequest({
                domain: 'survey_domain',
                url: '/evaluations' + ($scope.param.id ? ('/' + $scope.param.id) : ''),
                method: $scope.param.id ? 'PUT' : 'POST',
                scope: $scope,
                data: $scope.param,
                success: function (json) {
                    widget.msgToast('发布成功！', 500);
                    $rootScope.reset();
                    $state.go(con.state.main + '.evaluation.list');
                },
            })
        }
    };
});
