// This is a file copied by your subgenerator
/**
 * 默认的产品首页和默认的跳转规则
 */
define([
        '../states'
        , '../../cons/simpleCons'
        , '../../controllers/survey/questionController'
    ],
    function (stateModule, cons) {
        stateModule.config(
            ['$stateProvider', '$urlRouterProvider',
                function ($stateProvider, $urlRouterProvider) {
                    $stateProvider
                        .state(cons.state.main + '.question', {
                            url: "/question",
                            templateProvider: function ($templateCache) {
                                return $templateCache.get('app/' + cons.main_path + 'container.html');
                            }
                        })
                        .state(cons.state.main + '.question.list', {
                            url: "/list",
                            views: {
                                "": {
                                    templateProvider: function ($templateCache) {
                                        return '<div hjm-grid modid="questionList" config="config" columns="columns"></div>';
                                    }
                                }
                            }
                        })
                        .state(cons.state.main + '.question.add', {
                            url: "/add.html/:id",
                            views: {
                                "": {
                                    controller: 'question.updateController',
                                    templateProvider: function ($templateCache) {
                                        return $templateCache.get('app/' + cons.survey_path + 'question/update.html');
                                    }
                                }
                            }
                        })
                        .state(cons.state.main + '.question.update', {
                            url: "/update.html/:id",
                            views: {
                                "": {
                                    controller: 'question.updateController',
                                    templateProvider: function ($templateCache) {
                                        return $templateCache.get('app/' + cons.survey_path + 'question/update.html');
                                    }
                                }
                            }
                        })
                }
            ]);
    })
