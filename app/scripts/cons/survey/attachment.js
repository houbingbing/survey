define([], function () {
    var rtn = {
        attachmentList: {
            columns: [
                {name: 'ID', field: 'id', className: 'text-center'},
                {name: '类型', field: 'type', filter: 'survey_question_type'},
                {name: '描述', field: 'title'},
                {name: '上线时间', field: 'created_at'},
                {
                    name: '操作',className: 'text-center',
                    fieldDirective: '<span attachment-edit data="item"></span>&nbsp;&nbsp;&nbsp;' +
                    '<span attachment-del data="item" ></span>'
                },
            ],
            config: {
                title: '附加信息列表',
                domain: 'survey_domain',
                api: '/attachments',
                rowItemName: 'item',
                searchSupport: false,
                searchItems: [
                    {
                        value: 'status', text: '状态', type: 'btnGroup', default: '1', width: '6',
                        enum: [
                            {value: '', text: '全部'},
                            {value: '1', text: '正在进行'},
                            {value: '2', text: '暂停'},
                            {value: '3', text: '删除'},
                            {value: '4', text: '草稿'},
                        ]
                    },
                ],
                preSelectionSearch: {
                    age_min: '',
                    age_max: '',
                    status: '1',
                    category_type: '1'
                },
                paginationSupport: true,
                pageInfo: {
                    count: 20,
                    page: 1,
                    maxSize: 5, //最大展示页，默认3
                    // showPageGoto: false //属性为true将显示前往第几页。
                },
                route: [
                    // {value: 'main.attachment.add', text: '新增附加信息'},
                    {routeDirective: '<div attachment-add data="">新增附加信息</div>'},
                ]
            },
        }
    }
    return rtn;
});