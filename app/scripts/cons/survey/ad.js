define([], function () {
    var rtn = {
        adList: {
            columns: [
                {name: 'ID', field: 'id', className: 'text-center'},
                {name: '广告说明', field: 'name'},
                {name: '图片', fieldDirective: '<show_image url="item.image" style="width: 200px;" ></show_image> '},
                {name: '更新时间', field: 'updated_at'},
                {
                    name: '操作',className: 'text-center',
                    fieldDirective: '<div ad-edit data="item" ></div>&nbsp;&nbsp;&nbsp;' +
                    '<div ad-del data="item" ></div>&nbsp;&nbsp;&nbsp;'
                },
            ],
            config: {
                title: '推广列表',
                domain: 'survey_domain',
                api: '/ads',
                rowItemName: 'item',
                searchSupport: false,
                searchItems: [],
                preSelectionSearch: {
                    status: 1,
                    type: 2
                },
                paginationSupport: true,
                pageInfo: {
                    count: 20,
                    page: 1,
                    maxSize: 5, //最大展示页，默认3
                    // showPageGoto: false //属性为true将显示前往第几页。
                },
                route: [
                    // {value: 'main.product.add', text: '新增商品'},
                    {routeDirective: '<div ad-add data="">新增推广</div>'},
                ]
            },
        }
    }
    return rtn;
});