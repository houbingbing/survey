define([
    '../../directives/directives',
    '../../cons/simpleCons'
], function (mod, simpleCons) {
    mod
        .directive('surveyActivityEdit', function ($rootScope, $templateCache, $filter, $compile, widget) {
            return {
                multiElement: true,
                restrict: 'AE',
                replace: false,
                scope: {
                    data: '=',
                    attachment: '='
                },
                template: '',
                link: function ($scope, $element, $attrs) {
                    var content = '<a class="btn btn-success btn-rounded btn-sm"' +
                        'ui-sref="main.activity.update({id:' + $scope.data.id + '})">编辑</a>';
                    $element.html(content);
                    $compile($element.contents())($scope);
                }
            }
        })
        .directive('surveyActivityChangeStatus', function ($templateCache, $filter, $compile, widget, $uibModal, $timeout, $rootScope) {
            return {
                multiElement: true,
                restrict: 'AE',
                replace: false,
                scope: {
                    data: '=',
                },
                template: '',
                link: function ($scope, $element, $attrs) {
                    var status_text = '';
                    var click_text = '';
                    var class_text = '';
                    var status_title = '';
                    if ($scope.data.status == 1) {
                        status_title = '下架';
                        status_text = 'ng-bind="\'下架\'"';
                        class_text = 'ng-class={\"btn-warning\":true} ';
                        click_text = 'ng-click="change(2);"';
                        $scope.show_text = true;
                    } else if ($scope.data.status == 2 || $scope.data.status == 4) {
                        status_title = '上架';
                        status_text = 'ng-bind="\'上架\'"';
                        class_text = 'ng-class={\"btn-primary\":true} ';
                        click_text = 'ng-click="change(1);"';
                        $scope.show_text = true;
                    } else if ($scope.data.status == 3) {
                        $scope.show_text = false;
                    }
                    $scope.change = function (status) {
                        var supscope = $scope;
                        if (!confirm('确认修改为' + status_title + '状态?')) {
                            return false;
                        }
                        widget.ajaxRequest({
                            url: simpleCons.survey_domain + '/activities/' + supscope.data.id+'/status',
                            method: 'put',
                            scope: $scope,
                            data: {status: status},
                            success: function (json) {
                                widget.msgToast('修改成功,请刷新查看');
                                supscope.$parent.$parent.searchAction();
                            },
                        })
                    }
                    var content = '<a class="btn btn-rounded btn-sm"' + class_text + status_text + click_text +
                        ' ng-show="show_text"></a>';
                    $element.html(content);
                    $compile($element.contents())($scope);
                }
            }
        })
});
