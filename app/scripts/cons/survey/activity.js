define([], function () {
    var rtn = {
        activityList: {
            columns: [
                {name: 'ID', field: 'id', className: 'text-center'},
                {name: '标题', field: 'title', filter: 'zero2empty'},
                {
                    name: '参加条件', className: 'text-center',
                    fieldDirective: '<span ng-bind="item.limit_type|survey_limit_type"></span>' +
                    ' <span ng-bind="item.limit_day+\'天\'+item.limit_time+\'次\'" ng-show="item.limit_type==2"></span>' +
                    ' <span ng-show="item.limit_type==2 &&item.limit_loop==1">循环</span>     '
                },
                {name: '参与人数', field: 'stat_testing.user_count'},
                {name: '参与次数', field: 'stat_testing.count'},
                {
                    name: '活动日期', className: 'text-center',
                    fieldDirective: '<span ng-bind="item.start_time+\' ~ \'+item.end_time"></span>'
                },
                {
                    name: '当前状态',
                    field: 'status',
                    filter: 'start_end_time_status:item.start_time:item.end_time'
                },
                {
                    name: '操作', className: 'text-center',
                    fieldDirective: '<span survey-activity-edit data="item" ></span>&nbsp;&nbsp;&nbsp;' +
                    // '<span survey-activity-change-status data="item" ></span>' +
                    '<span survey-plan-download data="item"></span>',
                },
            ],
            config: {
                title: '活动列表',
                domain: 'survey_domain',
                api: '/activities',
                rowItemName: 'item',
                searchSupport: false,
                searchItems: [
                    // {
                    //     value: 'status', text: '状态', type: 'btnGroup', default: '1',
                    //     enum: [
                    //         {value: '', text: '全部'},
                    //         {value: '1', text: '正在进行'},
                    //         {value: '2', text: '已下线'},
                    //     ]
                    // }
                ],
                preSelectionSearch: {
                    // key: 'deviceNo',
                    // value: 'testinfo'
                },
                paginationSupport: true,
                pageInfo: {
                    count: 20,
                    page: 1,
                    maxSize: 5, //最大展示页，默认3
                    status: '1'
                    // showPageGoto: false //属性为true将显示前往第几页。
                },
                route: [
                    {value: 'main.activity.add', text: '新增测评活动'},
                ]
            },
        }
    }
    return rtn;
});