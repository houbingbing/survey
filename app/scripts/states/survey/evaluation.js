// This is a file copied by your subgenerator
/**
 * 默认的产品首页和默认的跳转规则
 */
define([
        '../states'
        , '../../cons/simpleCons'
        , '../../controllers/survey/evaluationController'
    ],
    function (stateModule, cons) {
        stateModule.config(
            ['$stateProvider', '$urlRouterProvider',
                function ($stateProvider, $urlRouterProvider) {
                    $stateProvider
                        .state(cons.state.main + '.evaluation', {
                            url: "/evaluation",
                            templateProvider: function ($templateCache) {
                                return $templateCache.get('app/' + cons.main_path + 'container.html');
                            }
                        })
                        .state(cons.state.main + '.evaluation.list', {
                            url: "/list",
                            views: {
                                "": {
                                    templateProvider: function ($templateCache) {
                                        return '<div hjm-grid modid="evaluationList" config="config" columns="columns"></div>';
                                    }
                                }
                            }
                        })
                        .state(cons.state.main + '.evaluation.add', {
                            url: "/add",
                            views: {
                                "": {
                                    controller: 'evaluation.updateController',
                                    templateProvider: function ($templateCache) {
                                        return $templateCache.get('app/' + cons.survey_path + 'evaluation/update.html');
                                    }
                                }
                            }
                        })
                        .state(cons.state.main + '.evaluation.update', {
                            url: "/update/:id",
                            views: {
                                "": {
                                    controller: 'evaluation.updateController',
                                    templateProvider: function ($templateCache) {
                                        return $templateCache.get('app/' + cons.survey_path + 'evaluation/update.html');
                                    }
                                }
                            }
                        })
                }
            ]);
    })
