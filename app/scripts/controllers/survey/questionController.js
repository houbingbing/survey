// This is a file copied by your subgenerator
define([
    './../controllers'
    , '../../cons/simpleCons'
], function (mod, con) {
    mod
        .controller('question.updateController', updateController)

    updateController.$injector = ['$scope', '$http', '$rootScope', '$uibModal', '$state', '$stateParams', 'widget', '$filter'];
    function updateController($scope, $http, $rootScope, $uibModal, $state, $stateParams, widget, comfunc, $filter) {
        if ($stateParams.id) {
            widget.ajaxRequest({
                domain: 'survey_domain',
                url: '/questions/' + $stateParams.id,
                method: 'get',
                scope: $scope,
                data: {},
                success: function (json) {
                    if ($state.current.name.indexOf('question.add') > -1) { // 复制用的
                        delete json.data.id;
                        delete json.data.status;
                        delete json.data.created_at;
                        delete json.data.updated_at;
                        angular.forEach(json.data.options, function (val, key) {
                            delete val.id;
                            delete val.question_id;
                            delete val.status;
                            delete val.created_at;
                            delete val.updated_at;
                        });
                    }
                    $scope.param = angular.copy(json.data);
                }
            })
        }
        $scope.submit = function (status) {
            // console.log($scope.param.contents);
            if (!$scope.param.title && !$scope.param.image) {
                widget.msgToast('题目,题图必须有其一才能提交');
                return false;
            }
            widget.ajaxRequest({
                domain: 'survey_domain',
                url: '/questions' + ($scope.param.id ? ('/' + $scope.param.id) : ''),
                method: $scope.param.id ? 'PUT' : 'POST',
                scope: $scope,
                data: $scope.param,
                success: function (json) {
                    widget.msgToast('发布成功！', 500);
                    $rootScope.reset();
                    $state.go(con.state.main + '.question.list');
                },
                failure: function (err) {
                    // console.log(err.code, Object.keys(err.validates), Object.keys(err.validates).length);
                    if ((err.code == 1202 || err.code == 1303) && err.validates && Object.keys(err.validates).length > 0) {
                        console.log(err.code, Object.keys(err.validates),
                            err.validates[Object.keys(err.validates)[0]],
                            Object.keys(err.validates).length);
                        widget.msgToast(err.validates[Object.keys(err.validates)[0]] + '\n点击取消', 5000);
                    } else {
                        widget.msgToast(err.message + '\n点击取消', 2000);
                    }
                }
            })
        }
    };
});
