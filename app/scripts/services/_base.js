define(['./services', '../cons/simpleCons', './widget', './comfunc'], function (mod, cons, widget) {
    mod
        .factory('bpmHttpInterceptor', ['$log', '$rootScope', function ($log, $rootScope) {
            // $log.debug('$log is here to show you that this is a regular factory with injection');
            var bpmHttpInterceptor = {
                'request': function (request) {
                    if (request.url.indexOf('.html') == -1) {
                        // console.log(config);
                        // console.log('config.url.indexOf(\'.html\') == -1 : ' + (config.url.indexOf('.html') == -1));
                        $rootScope.http_notification = '请求等待中...';
                    } else {
                        $rootScope.http_notification = '加载模板中...';
                    }
                    // console.log(request);
                    if (request.beforeSend) {
                        request.beforeSend();
                    }
                    return request;
                },
                'requestError': function (request) {
                    $rootScope.http_notification = null;
                    console.log('request  Error:  ' + request);
                },
                'response': function (response) {
                    $rootScope.http_notification = null;
                    if (response && response.data.code == 1000) {
                        delete $rootScope.hjm;
                        delete $rootScope.selected;
                        delete $rootScope.login_account;
                        localStorage.clear();
                        // $rootScope.$state.go('ads');
                        // response.data.message = '账号认证失败,重新登陆';
                    }
                    // console.log(response);
                    if (response.config.complete) {
                        response.config.complete(response);
                    }
                    return response;
                },
                'responseError': function (response) {
                    $rootScope.http_notification = null;
                    console.log('response  Error:  ' + response);
                    return response;
                }
            };
            return bpmHttpInterceptor;
        }])
        .run(['$rootScope', '$state', '$stateParams', '$http', '$uibModal', '$location', 'widget', '$document', '$q',
            function ($rootScope, $state, $stateParams, $http, $uibModal, $location, widget, $document, $q) {
                var arr = [];
                // 获取simpleCons.js 里的common 数据
                $rootScope.common = cons.common;
                $rootScope.env_name = cons.env_name;
                //活动公共数据
                // 获取公共数据 初始化
                $rootScope.nowlogintimestamp = new Date().getTime();
                $rootScope.lastlogintimestamp = JSON.parse(localStorage.getItem('lastlogintimestamp')) || 0;
                if (parseInt(($rootScope.nowlogintimestamp - $rootScope.lastlogintimestamp) / (1000 * 60 * 60 * 24)) > 1) {
                    //超过一天就更新
                    $rootScope.lastlogintimestamp = JSON.parse($rootScope.nowlogintimestamp);
                    localStorage.removeItem('hjm');
                } else {
                    localStorage.setItem('lastlogintimestamp', $rootScope.lastlogintimestamp = new Date().getTime());
                    $rootScope.hjm = JSON.parse(localStorage.getItem('hjm'));
                    $rootScope.current_city_name = $rootScope.hjm.current_city_name == '' ? '' : $rootScope.hjm.current_city_name;
                    $http.defaults.headers.common.Authorization = $rootScope.hjm.Authorization || '';
                }
                var deferred = $q.defer();
                var promise = deferred.promise;
                // 返回一个promise 对象
                var load_data = false;
                var reset_data_getting = false; //正在请求
                $rootScope.reset = function () {
                    var beforeSend_time = new Date().getTime();
                    var complete_time = '';
                    if (reset_data_getting) {
                        // 要是正在获取ajax 数据 在请求就直接等待 promise 就可以了
                        return deferred.promise;
                    } else {
                        load_data = widget.ajaxRequest({
                            // domain: 'web_domain',
                            url: location.protocol + '//' + location.host + ':' + location.port + '/get_common.php',
                            method: 'POST',
                            data: {
                                categorys: cons.survey_domain + '/categories?status=1&type=2&page=1&count=200',
                                classifications: cons.survey_domain + '/classifications?status=1&type=2&page=1&count=200',
                            },
                            beforeSend: function () {
                                reset_data_getting = true;
                            },
                            complete: function () {
                                reset_data_getting = false;
                            },
                            success: function (json) {
                                var survey_category = [];
                                angular.forEach(json.data.categorys, function (val, key) {
                                    survey_category[key] = {text: val.name, value: val.id + ''};
                                })

                                var survey_classification = [];
                                angular.forEach(json.data.classifications, function (val, key) {
                                    survey_classification[key] = {text: val.name, value: val.id + ''};
                                })

                                $rootScope.common['survey_category'] = survey_category;
                                $rootScope.common['survey_classification'] = survey_classification;
                                deferred.resolve(json);
                                // console.log(JSON.stringify(promise));
                            },
                            error: function (err) {
                                deferred.reject(err);
                                widget.msgToast('网络错误，请稍候再试');
                            }
                        })
                        return load_data.promise;
                    }

                }
                $rootScope.reset();
            }
        ])

});
