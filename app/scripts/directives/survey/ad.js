define([
    '../../directives/directives',
    '../../cons/simpleCons'
], function (mod, simpleCons) {
    mod
        .directive('adAdd', function ($rootScope, $templateCache, $filter, $compile, widget, $uibModal, $timeout) {
            return {
                restrict: 'AE',
                replace: true,
                scope: {
                    data: '=',
                },
                template: '<a class="btn btn-success btn-rounded btn-sm pull-right" ng-click="show_ad_add()" >新增推广</a>',
                link: function ($scope, $element, $attrs) {
                    var supscope = $scope;
                    $scope.show_ad_add = function () {
                        var modalInstance = $uibModal.open({
                                template: '<div modal-panel title="title" tmpl="tmpl"></div>',
                                controller: function ($scope, $uibModalInstance) {
                                    $scope.title = '新增推广';
                                    $scope.tmpl = '<form class="form-horizontal" name="FormBody" novalidate >' +
                                        '<div form-radio text="类型" type="radio" ng-model="param.type" required="true" default="1" ' +
                                        'source="[{text:\'链接\',value:\'1\'}]"source-api=""></div>' +
                                        '<div form-input text="文字内容" ng-model="param.name" required="true"></div>' +
                                        '<div form-image-single text="广告图" ng-model="param.image" required="true"></div>' +
                                        '<div form-input text="推广内容" ng-model="param.url" required="true"></div>' +
                                        '<a class="btn btn-success btn-rounded pull-right" ng-disabled="FormBody.$invalid" ng-click="submit()">确定</a>' +
                                        '</form>';
                                    $scope.submit = function () {
                                        widget.ajaxRequest({
                                            domain: 'survey_domain',
                                            url: '/ads',
                                            method: 'POST',
                                            scope: $scope,
                                            data: $scope.param,
                                            success: function (json) {
                                                widget.msgToast('推广维护成功,请刷新查看');
                                                $scope.$$prevSibling.$$childHead.$$nextSibling.$$nextSibling.$$nextSibling.$$childHead.$$childHead.searchAction()
                                                $scope.cancel();
                                            },
                                            failure: function (json) {
                                                widget.msgToast(json.message);
                                                $scope.cancel();
                                            }
                                        })
                                    }
                                    $scope.cancel = function () {
                                        $uibModalInstance.dismiss('cancel');
                                    };
                                },
                                size: 'lg'
                            }
                        );
                    }
                }
            }
        })
        .directive('adEdit', function ($rootScope, $templateCache, $filter, $compile, widget, $uibModal, $timeout) {
            return {
                restrict: 'AE',
                replace: true,
                scope: {
                    data: '=',
                },
                template: '<a class="btn btn-success btn-rounded btn-sm" ng-click="show_ad_edit()" >更新</a>',
                link: function ($scope, $element, $attrs) {
                    var supscope = $scope;
                    $scope.show_ad_edit = function () {
                        var modalInstance = $uibModal.open({
                                template: '<div modal-panel title="title" tmpl="tmpl"></div>',
                                controller: function ($scope, $uibModalInstance) {
                                    $scope.title = '更新推广';
                                    $scope.tmpl = '<form class="form-horizontal" name="FormBody" novalidate >' +
                                        '<div form-input text="ID" ng-model="param.id" ng-disabled="true"></div>' +
                                        '<div form-input text="文字内容" ng-model="param.name" required="true"></div>' +
                                        '<div form-radio text="类型" type="radio" ng-model="param.type" required="true" default="1" ' +
                                        'source="[{text:\'链接\',value:\'1\'}]"source-api=""></div>' +
                                        '<div form-image-single text="广告图" ng-model="param.image" required="true"></div>' +
                                        '<div form-input text="推广内容" ng-model="param.url" required="true"></div>' +
                                        '<a class="btn btn-success btn-rounded pull-right" ng-disabled="FormBody.$invalid" ng-click="submit()">确定</a>' +
                                        '</form>';
                                    $timeout(function () {
                                        $scope.param = angular.copy(supscope.data);
                                    }, 0);
                                    $scope.submit = function () {
                                        widget.ajaxRequest({
                                            domain: 'survey_domain',
                                            url: '/ads/' + supscope.data.id,
                                            method: 'PUT',
                                            scope: $scope,
                                            data: $scope.param,
                                            success: function (json) {
                                                widget.msgToast('推广维护成功,请刷新查看');
                                                $scope.$$prevSibling.$$childHead.$$nextSibling.$$nextSibling.$$nextSibling.$$childHead.$$childHead.searchAction()
                                                $scope.cancel();
                                            },
                                            failure: function (json) {
                                                widget.msgToast(json.message);
                                                $scope.cancel();
                                            }
                                        })
                                    }
                                    $scope.cancel = function () {
                                        $uibModalInstance.dismiss('cancel');
                                    };
                                },
                                size: 'lg'
                            }
                        );
                    }
                }
            }
        })
        .directive('adDel', function ($rootScope, $templateCache, $filter, $compile, widget, $uibModal, $timeout) {
            return {
                restrict: 'AE',
                replace: true,
                scope: {
                    data: '=',
                },
                template: '<a class="btn btn-danger btn-rounded btn-sm" ng-click="show_ad_del()" >删除</a>',
                link: function ($scope, $element, $attrs) {
                    var supscope = $scope;
                    $scope.show_ad_del = function () {
                        var modalInstance = $uibModal.open({
                                template: '<div modal-panel title="title" tmpl="tmpl"></div>',
                                controller: function ($scope, $uibModalInstance) {
                                    $scope.title = '删除测评推广';
                                    $scope.tmpl = '<form class="form-horizontal" name="FormBody" novalidate >' +
                                        '<div form-input text="ID" ng-model="param.id" ng-disabled="true"></div>' +
                                        '<div form-input text="文字内容" ng-model="param.name" ng-disabled="true"></div>' +
                                        '<div form-radio text="类型" type="radio" ng-model="param.type" required="true" default="1" ' +
                                        'source="[{text:\'链接\',value:\'1\'}]"source-api=""></div>' +
                                        '<div form-input text="推广内容" ng-model="param.url" ng-disabled="true"></div>' +
                                        '<a class="btn btn-danger btn-rounded pull-right" ng-click="submit()">删除</a>' +
                                        '</form>';
                                    $timeout(function () {
                                        // $scope.param = {id: supscope.data.id};
                                        $scope.param = supscope.data;
                                    }, 0);
                                    $scope.submit = function () {
                                        if (!$scope.param.name) {
                                            widget.msgToast('没有填写推广');
                                            return false;
                                        }
                                        widget.ajaxRequest({
                                            domain: 'survey_domain',
                                            url: '/ads/' + supscope.data.id,
                                            method: 'DELETE',
                                            scope: $scope,
                                            data: $scope.param,
                                            success: function (json) {
                                                widget.msgToast('删除推广成功,请刷新查看');
                                                $scope.$$prevSibling.$$childHead.$$nextSibling.$$nextSibling.$$nextSibling.$$childHead.$$childHead.searchAction()
                                                $scope.cancel();
                                            },
                                            failure: function (json) {
                                                widget.msgToast(json.message);
                                                $scope.cancel();
                                            }
                                        })
                                    }
                                    $scope.cancel = function () {
                                        $uibModalInstance.dismiss('cancel');
                                    };
                                },
                                size: ''
                            }
                        );
                    }
                }
            }
        })
});
