define([], function () {
    var rtn = {
        classificationList: {
            columns: [
                {name: 'ID', field: 'id', className: 'text-center'},
                // {name: '类型', field: 'type', filter: 'survey_classification_type'},
                {name: '分类', field: 'name'},
                {
                    name: '操作',className: 'text-center',
                    fieldDirective: '<div survey-classification-edit data="item" ></div>&nbsp;&nbsp;&nbsp;' +
                    '<div survey-classification-del data="item" ></div>&nbsp;&nbsp;&nbsp;'
                },
            ],
            config: {
                title: '题库分类列表',
                domain: 'survey_domain',
                api: '/classifications',
                rowItemName: 'item',
                searchSupport: false,
                searchItems: [],
                preSelectionSearch: {
                    status: 1,
                    type: 2
                },
                paginationSupport: true,
                pageInfo: {
                    count: 20,
                    page: 1,
                    maxSize: 5, //最大展示页，默认3
                    // showPageGoto: false //属性为true将显示前往第几页。
                },
                route: [
                    // {value: 'main.product.add', text: '新增商品'},
                    {routeDirective: '<div survey-classification-add data="">新增分类</div>'},
                ]
            },
        }
    }
    return rtn;
});