// This is a file copied by your subgenerator
/**
 * 默认的产品首页和默认的跳转规则
 */
define([
        '../states'
        , '../../cons/simpleCons'
        , '../../controllers/survey/planController'
    ],
    function (stateModule, cons) {
        stateModule.config(
            ['$stateProvider', '$urlRouterProvider',
                function ($stateProvider, $urlRouterProvider) {
                    $stateProvider
                        .state(cons.state.main + '.plan', {
                            url: "/plan",
                            templateProvider: function ($templateCache) {
                                return $templateCache.get('app/' + cons.main_path + 'container.html');
                            }
                        })
                        .state(cons.state.main + '.plan.list', {
                            url: "/list",
                            views: {
                                "": {
                                    // controller: 'accountController'
                                    templateProvider: function ($templateCache) {
                                        return '<div hjm-grid modid="planList" config="config" columns="columns"></div>';
                                    }
                                }
                            }
                        })
                        .state(cons.state.main + '.plan.add', {
                            url: "/add",
                            views: {
                                "": {
                                    controller: 'plan.updateController',
                                    templateProvider: function ($templateCache) {
                                        return $templateCache.get('app/' + cons.survey_path + 'plan/update.html');
                                    }
                                }
                            }
                        })
                        .state(cons.state.main + '.plan.update', {
                            url: "/update.html/:id",
                            views: {
                                "": {
                                    controller: 'plan.updateController',
                                    templateProvider: function ($templateCache) {
                                        return $templateCache.get('app/' + cons.survey_path + 'plan/update.html');
                                    }
                                }
                            }
                        })
                }
            ]);
    })
