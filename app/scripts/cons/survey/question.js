define([], function () {
    var rtn = {
        questionList: {
            columns: [
                {name: 'ID', field: 'id', className: 'text-center'},
                {
                    name: '题干文字', className: 'width300',
                    fieldDirective: '<span ng-bind="item.title"></span>'

                },
                {name: '维度', field: 'category.name'},
                {name: '分类', field: 'classification.name'},
                {
                    name: '年龄段',
                    fieldDirective: '<span ng-bind="item.age_min+\'-\'+item.age_max"></span>'
                },
                {name: '选项状态', className: 'text-center', fieldDirective: '<span question-option data="item"></span>'},
                {name: '发布时间', field: 'created_at'},
                {name: '状态', field: 'status|keyVal:\'1\':\'正常\':\'2\':\'停用\':\'3\':\'删除\':\'4\':\'草稿\''},
                {
                    name: '操作', className: 'text-center',
                    fieldDirective: '<span survey-question-edit data="item" ></span>&nbsp;&nbsp;&nbsp;' +
                    '<span survey-question-copy data="item" ></span>&nbsp;&nbsp;&nbsp;' +
                    // '<span survey-question-del data="item" ></span>&nbsp;&nbsp;&nbsp;' +
                    '<span survey-question-change-status data="item" ></span>'
                },
            ],
            config: {
                title: '题库列表',
                domain: 'survey_domain',
                api: '/questions',
                rowItemName: 'item',
                searchSupport: true,
                searchItems: [
                    {text: '关键字', value: 'keyword'},
                    {
                        text: '维度',
                        paramDirective: '<select class="form-control" ng-model="params.category_id" ' +
                        'ng-options="item.value as item.text for item in $root.common.survey_category">' +
                        '</select>'
                    },
                    {
                        value: 'status', text: '状态', type: 'btnGroup', default: '', width: '6',
                        enum: [
                            {value: '', text: '全部'},
                            {value: '1', text: '正常'},
                            {value: '2', text: '停用'},
                            // {value: '3', text: '删除'},
                            // {value: '4', text: '草稿'},
                        ]
                    },
                    // {
                    //     type: 'btnGroupArray',
                    //     value: 'flag',
                    //     text: '年龄段',
                    //     default: 0, //有enum_text 时 enumde index 的值
                    //     width: '6',
                    //     enum_text: ['age_min', 'age_max'],//  有  enum_text 说明是数组
                    //     enum: [
                    //         {value: ['', ''], text: '全部'},
                    //         {value: ['0', '3'], text: '0-3'},
                    //         {value: ['4', '6'], text: '4-6'},
                    //         {value: ['7', '9'], text: '7-9'},
                    //         {value: ['10', ''], text: '10岁以上'},
                    //     ]
                    // },
                ],
                preSelectionSearch: {
                    status: '',
                    // age_min: '',
                    // age_max: '',
                },
                paginationSupport: true,
                pageInfo: {
                    count: 20,
                    page: 1,
                    maxSize: 5, //最大展示页，默认3
                    // showPageGoto: false //属性为true将显示前往第几页。
                },
                route: [
                    {value: 'main.question.add', text: '新增题库'},
                    {routeDirective: '<span question-import></span>'},
                ],
                ext: {
                    showNum: [
                        {text: '总数', type: 'total'},
                    ]
                }
            },
        },
    }
    return rtn;
});