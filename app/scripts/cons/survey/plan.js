define([], function () {
    var rtn = {
        planList: {
            columns: [
                {name: 'ID', field: 'id', className: 'text-center'},
                {name: '标题', field: 'name', filter: 'zero2empty'},
                {name: '年龄', fieldDirective: '<span ng-bind="item.age_min +\'~\'+item.age_max"></span>        '},
                {name: '赛制', field: 'limit_type', filter: 'null2empty|keyVal:\'1\':\'标准赛\':\'2\':\'时间赛\''},
                {name: '赛制时间', field: 'limit_time+\'s\''},
                {
                    name: '当前状态/开关', className: 'text-center',
                    fieldDirective: '<h5><span ng-bind="item.status|keyVal:\'1\':\'正常\':\'2\':\'——\'"></span></h5>'
                },
                {
                    name: '操作', className: 'text-center',
                    fieldDirective: '<span survey-plan-edit data="item" ></span>&nbsp;&nbsp;&nbsp;' +
                    '<span survey-plan-change-status data="item"></span>&nbsp;&nbsp;&nbsp;' +
                    '<span survey-plan-download data="item"></span>',
                },
            ],
            config: {
                title: '测评列表',
                domain: 'survey_domain',
                api: '/plans',
                rowItemName: 'item',
                searchSupport: true,
                searchItems: [
                    {
                        value: 'status', text: '状态', type: 'btnGroup', default: '',
                        enum: [
                            {value: '', text: '全部'},
                            {value: '1', text: '正常'},
                            {value: '2', text: '停用'},
                        ]
                    }
                ],
                preSelectionSearch: {
                    // key: 'deviceNo',
                    // value: 'testinfo'
                },
                paginationSupport: true,
                pageInfo: {
                    count: 20,
                    page: 1,
                    maxSize: 5, //最大展示页，默认3
                    // showPageGoto: false //属性为true将显示前往第几页。
                },
                route: [
                    {value: 'main.plan.add', text: '新增测评'},
                ]
            },
        }
    }
    return rtn;
});