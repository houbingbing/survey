define([
    '../../directives/directives',
    '../../cons/simpleCons'
], function (mod, simpleCons) {
    mod
        .directive('questionOption', function (widget, $uibModal) {
            return {
                restrict: 'AE',
                replace: false,
                scope: {
                    data: '=',
                },
                template: '<a class="btn btn-rounded btn-sm btn-info" ng-click="show_option()">查看</a>',
                link: function ($scope, $element, $attrs) {
                    var supscope = $scope;
                    $scope.show_option = function () {
                        var modalInstance = $uibModal.open({
                                template: '<div modal-panel title="title" tmpl="tmpl"></div>',
                                controller: function ($scope, $uibModalInstance) {
                                    widget.ajaxRequest({
                                        domain: 'survey_domain',
                                        url: '/questions/' + (supscope.data.id || 0) + '/options',
                                        method: 'get',
                                        scope: $scope,
                                        data: {
                                            count: 200
                                        },
                                        success: function (json) {
                                            $scope.rtn_json = json.data;
                                        }
                                    })

                                    $scope.tmpl = '<div class="form-horizontal" name="FormBody" novalidate>' +
                                        ' <div form-table ng-model="rtn_json" config="{readonly:\'true\'}"' +
                                        'columns="[{\'name\': \'题干\', \'field\': \'name\',readonly:\'true\'},' +
                                        '{\'name\': \'分值\', \'field\': \'score\',readonly:\'true\'},' +
                                        '{\'name\': \'选择次数\', \'field\': \'selected\',readonly:\'true\'},' +
                                        '{\'name\': \'状态\', \'field\': \'status|keyVal:\\\'1\\\':\\\'正常\\\':\\\'2\\\':\\\'删除\\\'\',readonly:\'true\'},' +
                                        ']"></div>' +
                                        '</div>';
                                    $scope.title = '选项状态';
                                    $scope.cancel = function () {
                                        $uibModalInstance.dismiss('cancel');
                                    };
                                },
                                size: 'sm'
                            }
                        );
                    }
                }
            }
        })
        .directive('surveyQuestionEdit', function ($rootScope, $templateCache, $filter, $compile, widget) {
            return {
                multiElement: true,
                restrict: 'AE',
                replace: false,
                scope: {
                    data: '=',
                    attachment: '='
                },
                template: '',
                link: function ($scope, $element, $attrs) {
                    var content = '<a class="btn btn-success btn-rounded btn-sm"' +
                        'ui-sref="main.question.update({id:' + $scope.data.id + '})">编辑</a>';
                    $element.html(content);
                    $compile($element.contents())($scope);
                }
            }
        })
        .directive('surveyQuestionCopy', function ($rootScope, $templateCache, $filter, $compile, widget) {
            return {
                multiElement: true,
                restrict: 'AE',
                replace: false,
                scope: {
                    data: '=',
                    attachment: '='
                },
                template: '',
                link: function ($scope, $element, $attrs) {
                    var content = '<a class="btn btn-success btn-rounded btn-sm" ui-sref="main.question.add({id:' + $scope.data.id + '})">复制</a>';
                    $element.html(content);
                    $compile($element.contents())($scope);
                }
            }
        })
        .directive('surveyQuestionDel', function ($templateCache, $filter, $compile, widget, $rootScope) {
            return {
                multiElement: true,
                restrict: 'AE',
                replace: false,
                scope: {
                    data: '=',
                },
                template: '',
                link: function ($scope, $element, $attrs) {
                    var status_title = '删除';
                    var status_text = 'ng-bind="\'删除\'"';
                    var class_text = 'ng-class={\"btn-danger\":true} ';
                    var click_text = 'ng-click="change(3);"';
                    if ($scope.data.status != 3) {
                        $scope.show_text = true;
                    }
                    $scope.change = function (status) {
                        if (confirm('确认删除吗?')) {
                            widget.ajaxRequest({
                                url: '/surveys/questions/' + $scope.data.id || 0,
                                method: 'patch',
                                scope: $scope,
                                data: {status: status},
                                success: function (json) {
                                    widget.msgToast('删除成功,请刷新查看');
                                    $rootScope.reset();
                                    $scope.$parent.$parent.searchAction();
                                }
                            })
                        }
                    }
                    var content = '<a class="btn btn-rounded btn-sm"' + class_text + status_text + click_text +
                        ' ng-show="show_text"></a>';
                    $element.html(content);
                    $compile($element.contents())($scope);
                }
            }
        })
        .directive('surveyQuestionChangeStatus', function ($templateCache, $filter, $compile, widget, $uibModal, $timeout, $rootScope) {
            return {
                multiElement: true,
                restrict: 'AE',
                replace: false,
                scope: {
                    data: '=',
                },
                template: '',
                link: function ($scope, $element, $attrs) {
                    var status_text = '';
                    var click_text = '';
                    var class_text = '';
                    var status_title = '';
                    if ($scope.data.status == 1) {
                        status_title = '停用';
                        status_text = 'ng-bind="\'停用\'"';
                        class_text = 'ng-class={\"btn-warning\":true} ';
                        click_text = 'ng-click="change(2);"';
                        $scope.show_text = true;
                    } else if ($scope.data.status == 2 || $scope.data.status == 4) {
                        status_title = '正常';
                        status_text = 'ng-bind="\'正常\'"';
                        class_text = 'ng-class={\"btn-primary\":true} ';
                        click_text = 'ng-click="change(1);"';
                        $scope.show_text = true;
                    } else if ($scope.data.status == 3) {
                        $scope.show_text = false;
                    }
                    $scope.change = function (status) {
                        if (!confirm('确认修改为' + status_title + '状态?')) {
                            return false;
                        }
                        widget.ajaxRequest({
                            domain: 'survey_domain',
                            url: '/questions/' + $scope.data.id + '/status',
                            method: 'put',
                            scope: $scope,
                            data: {status: status},
                            success: function (json) {
                                widget.msgToast('修改成功,请刷新查看');
                                $scope.$parent.$parent.searchAction();
                                $rootScope.reset();
                            },
                            failure: function (json) {
                                widget.msgToast(json.message);
                            }
                        })
                    }
                    var content = '<a class="btn btn-rounded btn-sm"' + class_text + status_text + click_text +
                        ' ng-show="show_text"></a>';
                    $element.html(content);
                    $compile($element.contents())($scope);
                }
            }
        })
        .directive('questionImport', function ($templateCache, $filter, $compile, widget, $uibModal, $timeout, $rootScope, $http) {
            return {
                multiElement: true,
                restrict: 'AE',
                replace: true,
                scope: {
                    data: '=',
                },
                template: '<a class="btn btn-success btn-rounded btn-sm pull-right" style="margin-top: -5.5px;" ng-click="question_import()" >导入</a>',
                link: function ($scope, $element, $attrs) {
                    var supscope = $scope;
                    $scope.question_import = function () {
                        var modalInstance = $uibModal.open({
                                template: '<div modal-panel title="title" tmpl="tmpl"></div>',
                                controller: function ($scope, $uibModalInstance) {
                                    $scope.title = '导入题库';
                                    $scope.tmpl = '<form class="form-horizontal" name="FormBody" novalidate >' +
                                        ' <input type="file" class="form-control" id="question" name="question"><hr>' +
                                        '<a class="btn btn-primary btn-rounded pull-right" ng-disabled="FormBody.$invalid" ng-click="submit()">确定</a>' +
                                        '</form>';
                                    $scope.submit = function () {
                                        var formData = new FormData(); //初始化一个FormData实例
                                        formData.append('question', $('#question')[0].files[0]); //file就是图片或者其他你要上传的formdata，可以通过$("input")[0].files[0]来获取
                                        $http({
                                            method: 'POST',
                                            url: simpleCons.survey_domain + '/imports/questions',
                                            data: formData,
                                            headers: {'Content-Type': undefined}
                                        }).success(function (json) {
                                            if (json.code == 0) {
                                                widget.msgToast('导入成功,请刷新查看');
                                                $scope.$$prevSibling.$$childHead.$$nextSibling.$$nextSibling.$$nextSibling.$$childHead.$$childHead.searchAction();
                                                $scope.cancel();
                                            } else {
                                                widget.msgToast(json.message)
                                            }
                                        }).error(function (err) {
                                            widget.msgToast(err.message)
                                        });
                                    }
                                    $scope.cancel = function () {
                                        $uibModalInstance.dismiss('cancel');
                                    };
                                },
                                size: 'sm'
                            }
                        );
                    }
                }
            }
        })
});
